import { Component, Input, OnInit, Output, SimpleChanges, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { AppService } from '../app.service';
import { BsModalRef, BsModalService  } from 'ngx-bootstrap/modal'
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-cadastro',
  templateUrl: './cadastro.component.html',
})
export class CadastroComponent implements OnInit {
  public ordensServico: any;
  public cadastroForm = this.formBuilder.group({
    numero: ["" , Validators.required],
    titulo: ["", Validators.required],
    cnpjCliente: ["", Validators.required],
    nomeCliente: ["", Validators.required],
    cpfPrestadorServico: ["", Validators.required],
    nomePrestadorServico: ["", Validators.required],
    valor: ["", Validators.required],
    dataExecucao: ["", Validators.required]
  });
  public novaOrdem: any;

  @Input() ordemSelecionada: any = [];
  @Output() onAtualizarTela = new EventEmitter<boolean>();
  @Input() edicao: boolean = false;
  @Input() cadastro: boolean = false;
  @Output() onFecharModal = new EventEmitter<boolean>();
  @Output() title : string = "";
  
  constructor(private appService: AppService, private bsModalRef: BsModalRef, private bsModalService : BsModalService, private formBuilder: FormBuilder){}

  ngOnInit(){ 
    this.iniciarFormulario();
  }

  atualizarTela(value: boolean) {
    this.bsModalRef.hide();
    this.onAtualizarTela.emit(value);
  }

  iniciarFormulario() : void{
    if(this.edicao){
      this.preencherFormulario(this.ordemSelecionada);
    }
  }

  salvarFormulario() : void {
    if(this.edicao){
      this.appService.editar(this.cadastroForm.value)
      .subscribe(response => {
        this.atualizarTela(true);
      }, error => {
        alert("Ocorreu um erro ao editar a ordem de serviço! Verifique se todos os campos estão preenchidos corretamente.");
      });
    }else if(this.cadastro) {
      this.appService.inserir(this.cadastroForm.value)
      .subscribe(response => {
        this.atualizarTela(true);
      }, error => {
        alert("Ocorreu um erro ao cadastrar a ordem de serviço! Verifique se todos os campos estão preenchidos corretamente.");
      });
     
    }
    
  }

  preencherFormulario(ordem : any){
    this.appService.recuperar(ordem.id)
    .subscribe((response : any) => {
      this.ordemSelecionada = response;
      this.cadastroForm.patchValue({
        numero: this.ordemSelecionada.numero,
        titulo: this.ordemSelecionada.titulo,
        cnpjCliente: this.ordemSelecionada.cnpjCliente,
        nomeCliente: this.ordemSelecionada.nomeCliente,
        cpfPrestadorServico: this.ordemSelecionada.cpfPrestadorServico,
        nomePrestadorServico: this.ordemSelecionada.nomePrestadorServico, 
        valor: this.ordemSelecionada.valor,
        dataExecucao: this.ordemSelecionada.dataExecucao
      });

      this.cadastroForm.updateValueAndValidity({ onlySelf: true });
    });
  }

  fecharModal() {
    this.bsModalRef.hide();
    this.onFecharModal.emit(true);
  }
}