import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class AppService {
    public url:string = "http://localhost:44345/ordemservico/";
  constructor(private http:HttpClient) { }

  listar() {
    return this.http.get(this.url);
  }

  recuperar(id :number){
      return this.http.get(this.url + id);
  }

  deletar(id :number){
    return this.http.delete(this.url + id);
  }

  editar(request : any){
    return this.http.put(this.url, request);
  }

  inserir(request : any){
    return this.http.post(this.url, request);
  }
}