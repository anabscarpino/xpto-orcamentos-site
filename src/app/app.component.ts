import { Component, EventEmitter } from '@angular/core';
import { AppService } from './app.service';
import { BsModalRef, BsModalService  } from 'ngx-bootstrap/modal'
import { CadastroComponent } from './cadastro/cadastro.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'xpto-orcamentos-site';
  public ordensServico: any;
  public ordemSelecionada: any;
  public edicao: boolean = false;
  public cadastro: boolean = false;
  constructor(private appService: AppService, private bsModalRef: BsModalRef, private bsModalService : BsModalService){}

  ngOnInit(){
    this.listar();
  }

  listar(): void {
    this.appService.listar().subscribe(response =>{
      this.ordensServico = response;
    });
  }

  cadastrarOs() {
    this.cadastro = true;
    const initialState = { edicao: this.edicao, cadastro: this.cadastro, title: "Cadastrar nova OS"}
    this.bsModalRef = this.bsModalService.show(CadastroComponent, { initialState });
    this.bsModalRef.content.onAtualizarTela.subscribe((data : boolean) => {
      this.atualizarTela(data);
    });
    this.bsModalRef.content.onFecharModal.subscribe((data : boolean) => {
      this.fecharModal(data)
    });
  }

  editarOs(ordem : any){
    this.edicao =true;
    this.ordemSelecionada = ordem;
    const initialState = { edicao: this.edicao, cadastro: this.cadastro , ordemSelecionada: this.ordemSelecionada, title: "Editar OS"}
    this.bsModalRef = this.bsModalService.show(CadastroComponent, { initialState });

    this.bsModalRef.content.onAtualizarTela.subscribe((data : boolean) => {
      this.atualizarTela(data);
    });
    this.bsModalRef.content.onFecharModal.subscribe((data : boolean) => {
      this.fecharModal(data)
    });
    
  }

  atualizarTela(value: boolean) {
    this.cadastro = false;
    this.edicao = false;
    this.listar();
  }

  excluirOs(ordem : any){
    if(ordem != undefined){
      this.appService.deletar(ordem.id).subscribe(response => {
        this.atualizarTela(true);
      }, error => {
          alert("Ocorreu um erro ao excluir a ordem de serviço!");
      });
    }
  }
  
  fecharModal(value: boolean) {
    this.cadastro = false;
    this.edicao = false;
  }
}
